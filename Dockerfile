FROM harbor.gretzki.ddns.net/docker-cache/ubuntu:24.10
COPY scripts/runtime.sh ./
ENV DEBIAN_FRONTEND=noninteractive
RUN export DEBIAN_FRONTEND=noninteractive \
&& apt-get update \
&& apt-get -y upgrade \
&& apt-get install openssh-server sudo git curl openjdk-21-jre dotnet-sdk-9.0 aspnetcore-runtime-9.0 -y --no-install-recommends \
&& dotnet --info \
&& java --version \
&& service ssh start \
&& chmod +x runtime.sh
EXPOSE 22
ENTRYPOINT ["./runtime.sh"] 
